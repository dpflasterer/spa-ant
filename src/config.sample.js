// config.js
module.exports = {
  'app' : {
    'port' : 3000
  },
  'db' : {
    'name' : '<db name>',
    'url' : 'mongodb://localhost/<db name>' // looks like 'mongodb://<login>:<password>@mongohq.com:<port>/<database name>'
  },
  'mysql': {
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'spa-ant'
  },
  'session' : {
    'key' : 'connect.sid',
    'secret' : 'sweetsweetsessionkey',
    'maxAge' : 3600000 * 24 //24 hours
  }
};
