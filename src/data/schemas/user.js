var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// Javascript RFC2822 Email Validation
var emailRegexp = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/; // jshint ignore:line

var userSchema = mongoose.Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	name: String,
	password: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true,
		unique: true,
		match: emailRegexp
	},
	role: {
		type: String,
		required: true,
		default: 'default',
		enum: [
		'owner',
		'admin',
		'member',
		'default'
		]
	},
	btceAPI: String
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.password);
};

// remove the password
userSchema.methods.toJSON = function() {
	var obj = this.toObject();
	delete obj.password;
	return obj;
};

// give the user role a numeric value
userSchema.methods.roleLevel = function() {
	var x = null;
	switch (this.role){
		case 'default':
			x = 0;
			break;
		case 'member':
			x = 1;
			break;
		case 'admin':
			x = 99;
			break;
		case 'owner':
			x = 100;
			break;
		default:
			x = 0;
	}
	return x;
};

module.exports = userSchema;
