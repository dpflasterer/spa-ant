(function() {
	'use strict';

	angular
		.module('app.core',
		['blocks.logger', 'blocks.router', 'blocks.exception',
			'ngAnimate', 'ngplus', 'ngSanitize']);
	//'ui.router',

	var core = angular.module('app.core');

	core.config(toastrConfig);

	toastrConfig.$inject = ['toastr'];

	function toastrConfig(toastr) {
		toastr.options.timeOut = 4000;
		toastr.options.positionClass = 'toast-bottom-right';
	}

	var config = {
		appErrorPrefix: '[spa-ant Error] ',
		appTitle: 'spa-ant'
	};

	core.value('config', config);
})();
