(function() {
	'use strict';

	angular
		.module('app')
		.run(appRun);

	function appRun(routerHelper) {
		var otherwise = '/404';
		routerHelper.configureStates(getStates(), otherwise);
	}

	function getStates() {
		return [
			{
				state: 'home',
				config: {
					url: '/',
					templateUrl: 'home/home.html',
					//controller: 'HomeController',
					//controllerAs: 'vm',
					title: 'home',
					settings: {
						nav: 1,
						content: '<i class="fa fa-home"></i> Home'
					}
				}
			}
		];
	}

})();
