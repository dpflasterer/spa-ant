(function() {
	'use strict';

	angular
		.module('app.admin')
		.controller('AdminController', ['adminService', 'adminDataService', 'layoutService',
			function(adminService, adminDataService, layoutService) {
				var vm = this;

				vm.setIndex = setIndex;
				vm.isIndex = isIndex;
				vm.isAdmin = isAdmin;
				vm.showAdmin = showAdmin;
				vm.users = getUsers;
				vm.reloadUsers = reloadUsers;
				vm.updateUser = updateUser;

				////////////////////

				function setIndex(i) {
					layoutService.setAdminIndex(i || 0);
				}

				function isIndex(i) {
					return layoutService.isAdminIndex(i);
				}

				function isAdmin() {
					return adminService.isAdmin();
				}

				function showAdmin() {
					return adminService.showAdmin();
				}

				function getUsers() {
					return adminDataService.getUsers();
				}

				function reloadUsers() {
					adminDataService.reloadUsers();
				}

				function updateUser(userId) {
					adminDataService.updateUser(userId);
				}

			}]);

})();
