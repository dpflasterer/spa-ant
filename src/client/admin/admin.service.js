(function() {
	'use strict';

	angular
		.module('app.admin')
		.service('adminService', function() {
			var adminStatus = false;
			var showAdminStatus = false;

			var admin = {
				isAdmin: isAdmin,
				showAdmin: showAdmin,
				setAdminStatus: setAdminStatus,
				toggleAdmin: toggleAdmin
			};

			return admin;

			////////////////////

			function setAdminStatus(status) {
				adminStatus = status;
			}

			function isAdmin() {
				return adminStatus;
			}

			function toggleAdmin() {
				if (adminStatus) {
					showAdminStatus = !showAdminStatus;
				}
			}

			function showAdmin() {
				return adminStatus && showAdminStatus;
			}

		});

})();
