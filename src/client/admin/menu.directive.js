(function() {
	'use strict';

	angular
		.module('app.admin')
		.directive('adminMenu', function adminMenu() {
			return {
				bindToController: true,
				restrict: 'E',
				templateUrl: 'admin/menu.html',
				controller: 'AdminController',
				controllerAs: 'admin'
			};

		});

})();
