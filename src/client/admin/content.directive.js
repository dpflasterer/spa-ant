(function() {
	'use strict';

	angular
		.module('app.admin')
		.directive('adminContent', function() {
			return {
				restrict: 'E',
				templateUrl: 'admin/content.html',
				controller: 'AdminController',
				controllerAs: 'admin'
			};
		});

})();
