(function() {
	'use strict';

	angular
		.module('app.admin')
		.directive('adminUsers', function() {
			return {
				restrict: 'E',
				templateUrl: 'admin/users.html'
			};
		});

})();
