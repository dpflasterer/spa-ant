(function() {
	'use strict';

	angular
		.module('app.session')
		.controller('SessionController', ['$scope', 'adminService', 'sessionDataService', 'layoutService',
			function($scope, adminService, sessionDataService, layoutService) {
			var vm = this;

			// defaults
			vm.formData = {
				username: '',
				email: '',
				name: '',
				password: '',
				password2: ''
			};

			vm.user = user;
			vm.clearAlert = clearAlert;
			vm.alert = alert;

			vm.setIndex = setIndex;
			vm.isIndex = isIndex;

			vm.isAdmin = isAdmin;
			vm.toggleAdmin = toggleAdmin;
			vm.showAdmin = showAdmin;

			vm.signUp = signUp;
			vm.login = login;
			vm.logout = logout;
			vm.update = update;
			vm.check = check;

			// initialize
			vm.check();

			////////////////////

			function setIndex(i) {
				clearAlert();
				layoutService.setSessionIndex(i || 0);
			}

			function isIndex(i) {
				return layoutService.isSessionIndex(i);
			}

			////////////////////  Admin Helpers

			function toggleAdmin() {
				return adminService.toggleAdmin();
			}

			function showAdmin() {
				return adminService.showAdmin();
			}

			function isAdmin() {
				return adminService.isAdmin();
			}

			function setAdminStatus() {
				var status = vm.user() && (vm.user().role === 'admin' || vm.user().role === 'owner');
				return adminService.setAdminStatus(status);
			}

			/**
			 * Getters
			 */
			function user() {
				return sessionDataService.user();
			}

			function alert() {
				return sessionDataService.alert();
			}

			function clearAlert() {
				return sessionDataService.clearAlert();
			}

			/**
			 * Methods
			 */
			function signUp() {
				return sessionDataService.signUp(vm.formData)
					.then(function() {
						setIndex(0);
						setAdminStatus();
					});
			}

			function login() {
				return sessionDataService.login(vm.formData)
					.then(function() {
						setIndex(0);
						setAdminStatus();
					});
			}

			function logout() {
				return sessionDataService.logout()
					.then(function() {
						setIndex(0);
						setAdminStatus();
					});
			}

			function update() {
				return sessionDataService.update(vm.user())
					.then(function() {
						setAdminStatus();
					});
			}

			function check() {
				return sessionDataService.check()
					.then(function() {
						setAdminStatus();
					});
			}

		}]);

})();
