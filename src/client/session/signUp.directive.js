(function() {
	'use strict';

	angular
		.module('app.session')
		.directive('sessionSignUp', function() {
			return {
				restrict: 'E',
				templateUrl: 'session/sign-up.html'
			};
		});

})();

