(function() {
	'use strict';

	angular
		.module('app.session')
		.directive('sessionLogin', function() {
			return {
				restrict: 'E',
				templateUrl: 'session/login.html'
			};
		});

})();
