(function() {
	'use strict';

	angular
		.module('app.session')
		.directive('sessionMenu', function() {
			return {
				restrict: 'E',
				templateUrl: 'session/menu.html',
				controller: 'SessionController',
				controllerAs: 'session'
			};
		});

})();

