(function() {
	'use strict';

	angular
		.module('app.session', ['app.services', 'app.layout', 'app.admin']);

})();
