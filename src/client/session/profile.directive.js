(function() {
	'use strict';

	angular
		.module('app.session')
		.directive('sessionProfile', function() {
			return {
				restrict: 'E',
				templateUrl: 'session/profile.html'
			};
		});

})();
