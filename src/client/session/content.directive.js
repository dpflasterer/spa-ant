(function() {
	'use strict';

	angular
		.module('app.session')
		.directive('sessionContent', function() {
			return {
				restrict: 'E',
				templateUrl: 'session/content.html',
				controller: 'SessionController',
				controllerAs: 'session'
			};
		});

})();

