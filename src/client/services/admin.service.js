(function() {
	'use strict';

	angular
		.module('app.services')
		.service('adminDataService', ['$http', '$q', 'logger',
			function($http, $q, logger) {
				var users = null;
				var usersLoaded = false;

				var admin = {
					getUsers: getUsers,
					reloadUsers: reloadUsers,
					updateUser: updateUser
				};

				// init
				loadUsers();

				return admin;

				////////////////////

				function getUsers() {
					return users;
				}

				function loadUsers() {
					var defrd = $q.defer();
					$http.get('/users')
						.success(function(data, status, headers, config) {
							if (data.success) {
								users = data.users;
								usersLoaded = true;
								logger.info('Users Loaded');
								defrd.resolve(true);
							} else {
								defrd.reject();
							}
						})
						.error(function(data, status, headers, config) {
							defrd.reject();
						});

					return defrd.promise;
				}

				function reloadUsers() {
					usersLoaded = false;
					loadUsers();
				}

				function updateUser(data) {
					var defrd = $q.defer();
					$http.post('/users/' + data.username, data)
						.success(function(data, status, headers, config) {
							if (data.success) {
								logger.info(data.message);
								defrd.resolve();
							} else {
								logger.error(data.message);
								defrd.reject();
							}
						})
						.error(function(data, status, headers, config) {
							logger.warning('There was an error communicating with the server.');
						});

					return defrd.promise;
				}

			}]);

})();
