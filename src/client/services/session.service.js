(function() {
	'use strict';

	angular
		.module('app.services')
		.service('sessionDataService', ['$http', '$q',
			function($http, $q) {
				var user = null;
				var alert = null;

				var session = {
					user: getUser,
					alert: getAlert,
					clearAlert: clearAlert,
					login: login,
					logout: logout,
					signUp: signUp,
					update: update,
					check: check
				};

				return session;

				////////////////////

				function getUser() {
					return user;
				}

				function getAlert() {
					return alert;
				}

				function clearAlert() {
					alert = null;
				}

				function login(data) {
					var defrd = $q.defer();

					$http.post('/session', data)
						.success(function(data, status, headers, config) {
							if (data.success) {
								user = data.user;
								defrd.resolve();
							} else {
								alert = {
									label: 'danger',
									message: data.message
								};
								defrd.reject();
							}
						})
						.error(function(data, status, headers, config) {
							alert = {
								label: 'danger',
								message: 'There was an error communicating with the server.'
							};
							defrd.reject();
						});

					return defrd.promise;
				}

				function logout() {
					var defrd = $q.defer();

					user = null;
					$http.delete('/session').
						success(function(data, status, headers, config) {
							defrd.resolve();
						}).
						error(function(data, status, headers, config) {
							defrd.reject();
						});

					return defrd.promise;
				}

				function signUp(data) {
					var defrd = $q.defer();
					$http.post('/users/new', data)
						.success(function(data, status, headers, config) {
							if (data.success) {
								user = data.user;
								defrd.resolve();
							} else {
								alert = {
									label: 'danger',
									message: data.message
								};
								defrd.reject();
							}
						})
						.error(function(data, status, headers, config) {
							alert = {
								label: 'danger',
								message: 'There was an error communicating with the server.'
							};
							defrd.reject();
						});

					return defrd.promise;
				}

				function update(data) {
					var defrd = $q.defer();
					$http.post('/users/' + data.username, data)
						.success(function(data, status, headers, config) {
							if (data.success) {
								alert = {
									label: 'info',
									message: data.message
								};
								user = data.user;
								defrd.resolve();
							} else {
								alert = {
									label: 'danger',
									message: data.message
								};
								defrd.reject();
							}
						})
						.error(function(data, status, headers, config) {
							alert = {
								label: 'danger',
								message: 'There was an error communicating with the server.'
							};
						});

					return defrd.promise;
				}

				function check() {
					var defrd = $q.defer();
					$http.get('/session')
						.success(function(data, status, headers, config) {
							if (data.success) {
								user = data.user;
							}
							defrd.resolve(true);
						})
						.error(function(data, status, headers, config) {
							defrd.reject();
						});

					return defrd.promise;
				}

			}]);

})();
