/**
 * name: spa-ant
 * version: 0.0.1
 * description: Single Page Application; Angular and Node Template
 * Lead Developer: David Pflasterer
 * Version: 0.0.1
 */

(function() {
	'use strict';

	console.log('v0.0.1');

	angular
		.module('app', ['app.templates', 'app.core', 'app.layout', 'app.services', 'app.session', 'app.admin'])

		.directive('users', function() {
			return {
				restrict: 'E',
				templateUrl: 'primary/users.html'
			};
		});

})();

