(function() {
	'use strict';

	angular
		.module('app.layout')
		.service('layoutService', function() {
			var adminIndex = 0;
			var adminStatus = false;
			var sessionIndex = 0;
			var sessionStatus = false;

			var layout = {
				isSidebarActive: isSidebarActive,
				isAdminIndex: isAdminIndex,
				setAdminIndex: setAdminIndex,
				isSessionIndex: isSessionIndex,
				setSessionIndex: setSessionIndex
			};

			return layout;

			////////////////////  Sidebar

			function isSidebarActive() {
				return adminStatus || sessionStatus;
			}

			//////////////////// Admin

			function isAdminIndex(i) {
				return adminIndex === i;
			}

			function setAdminIndex(i) {
				adminIndex = i;
				setAdminStatus(i > 0);
			}

			function setAdminStatus(status) {
				adminStatus = status;
			}

			//////////////////// Session

			function isSessionIndex(i) {
				return sessionIndex === i;
			}

			function setSessionIndex(i) {
				sessionIndex = i;
				setSessionStatus(i > 0);
			}

			function setSessionStatus(status) {
				sessionStatus = status;
			}

		});

})();
