(function() {
	'use strict';

	angular
		.module('app.layout')
		.controller('ShellController', [
			'$rootScope', '$scope', '$timeout', 'config', 'logger',
			'layoutService',
			function($rootScope, $scope, $timeout, config, logger, layoutService) {
				var vm = this;

				vm.busyMessage = 'Please wait ...';
				vm.isBusy = true;
				$rootScope.showSplash = true;
				vm.navline = {
					title: config.appTitle,
					text: 'Created by DPflasterer',
					link: 'http://davidpflasterer.com'
				};
				vm.contentColumns = contentColumns;
				vm.sidebarColumns = sidebarColumns;
				vm.isSidebarActive = isSidebarActive;

				activate();

				///////////////////////

				function activate() {
					logger.success(config.appTitle + ' loaded!', null);
					hideSplash();
					isSidebarActive();
				}

				function hideSplash() {
					//Force a 1 second delay so we can see the splash.
					$timeout(function() {
						$rootScope.showSplash = false;
					}, 1000);
				}

				///////////////////////

				function contentColumns() {
					return isSidebarActive() ? ['col-sm-9'] : ['col-sm-12'];
				}

				function sidebarColumns() {
					return isSidebarActive() ? ['col-sm-3'] : ['hide', 'col-sm-0'];
				}

				function isSidebarActive() {
					return layoutService.isSidebarActive();
				}
			}]);

})();
