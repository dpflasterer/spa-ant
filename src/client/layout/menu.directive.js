(function() {
	'use strict';

	angular
		.module('app.layout')
		.directive('primaryContentMenu', function primaryContentMenu() {
			return {
				bindToController: true,
				restrict: 'E',
				templateUrl: 'layout/menu.html',
				controller: 'MenuController',
				controllerAs: 'vm'
			};

		});

})();
