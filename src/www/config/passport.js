var LocalStrategy = require('passport-local').Strategy;

// load up the user model
var User = require('../../data/models/user');

// expose this function to our app using module.exports
module.exports = function(passport) {

	// =========================================================================
	// passport session setup ==================================================
	// =========================================================================
	// required for persistent login sessions
	// passport needs ability to serialize and unserialize users out of session

	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	// used to deserialize the user
	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	// =========================================================================
	// LOCAL SIGNUP ============================================================
	// =========================================================================
	// we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

	passport.use('local-signup', new LocalStrategy({
		// by default, local strategy uses username and password, we will override with email
		usernameField : 'username',
		passwordField : 'password',
		passReqToCallback : true // allows us to pass back the entire request to the callback
	},
  function(req, username, password, done) {

	// asynchronous
	// User.findOne wont fire unless data is sent back
	process.nextTick(function() {

		// find a user whose email is the same as the forms email
		// we are checking to see if the user trying to login already exists
		User.findOne({'username' :  username}, function(err, user) {
			// if there are any errors, return the error
			if (err) {
				return done(err);
			}

			// check to see if theres already a user with that email
			if (user) {
				return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
			} else {

				// if there is no user with that email
				// create the user
				var newUser = new User();

				// set the user's local credentials
				newUser.username = username;
				newUser.name = req.body.name;
				newUser.password = newUser.generateHash(password);
				newUser.email = req.body.email;
				newUser.btceAPI = req.body.btceAPI;

				// save the user
				newUser.save(function(err) {
					if (err) { console.log(err); } // todo: try to prevent all errors

					req.session.user = {
            'id': newUser._id,
            'username': newUser.username,
            'name': newUser.name};
					return done(null, newUser);
				});

			}
		});
	});
  }));

	// =========================================================================
	// LOCAL LOGIN =============================================================
	// =========================================================================
	// we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

	passport.use('local-login', new LocalStrategy({
		// by default, local strategy uses username and password, we will override with email
		usernameField : 'username',
		passwordField : 'password',
		passReqToCallback : true // allows us to pass back the entire request to the callback
	},
    function(req, username, password, done) { // callback with email and password from our form

	// find a user whose email is the same as the forms email
	// we are checking to see if the user trying to login already exists
	User.findOne({'username' :  username}, function(err, user) {
		// if there are any errors, return the error before anything else
		if (err) {
			return done(err);
		}

		// if no user is found, return the message
		if (!user) {
			// req.flash is the way to set flashdata using connect-flash
			return done(null, false, req.flash('loginMessage', 'No user found.'));
		}

		// if the user is found but the password is wrong
		if (!user.validPassword(password)) {
			// create the loginMessage and save it to session as flashdata
			return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
		}

		// all is well, set session data and return successful user
		req.session.user = {'id': user._id,
      'username': user.username,
      'name': user.name};
		return done(null, user);
	});

    }));
};
