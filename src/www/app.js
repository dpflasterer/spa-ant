/**
 * Name: SPA Template- Node
 * Lead Developer: David Pflasterer
 * Version: 0.0.1
 */

var http = require('http');
var express = require('express');
var session = require('express-session');

var flash = require('connect-flash');
var mongoStore = require('connect-mongo')(session);

var serveStatic = require('serve-static');
var favicon = require('serve-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');

var path = require('path');
var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config.js');

var usersRoute = require('./routes/users.js');
var sessionRoute = require('./routes/session.js');

var app = express();

var port = process.env.PORT || config.app.port;
var environment = process.env.NODE_ENV;

console.log('Staring up node');
console.log('PORT=' + port);
console.log('NODE_ENV=' + environment);

// configuration ===============================================================
mongoose.connect(config.db.url); // connect to our database

require('./config/passport')(passport);

// all environments
app.set('port', port);
//app.set('views', __dirname);// path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.set('view options', { layout: false });
//app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(methodOverride());
app.use(cookieParser());
app.use(session({
	resave: true,
	saveUninitialized: true,
	key: config.session.key,
	secret: config.session.secret,
	maxAge: config.session.maxAge,
	store: new mongoStore({
		db: config.db.name
	})
}));
app.use(flash()); // use connect-flash for flash messages stored in session

// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next) {
	res.locals.session = req.session;
	next();
});

//development only
if ('build' === environment) {
	app.use(serveStatic(path.join(__dirname, '../../public')));
	app.get('*', function(req, res) {
		res.sendFile(path.join(__dirname, '../../public/index.html'));
	});
} else {
	app.use(errorHandler({dumpExceptions: true, showStack: true}));
	app.use(serveStatic(path.join(__dirname, '../../')));
	app.use(serveStatic(path.join(__dirname, '../../dev')));
	app.get('*', function(req, res) {
		res.sendFile(path.join(__dirname, '../../dev/index.html'));
	});

}

app.use('/users', usersRoute);
app.use('/session', sessionRoute);

var server = http.createServer(app);
server.listen(port, function() {
	console.log('Express server listening on port ' + port);
});
