/**
 * Created by David on 4/6/14.
 */

// route middleware to make sure a user is logged in
function privilegesOnly(role) {
	return function(req, res, next) {
		console.log(req.user.name + ': ' + req.user.role + ' : ' + role);
		if (role === 'admin' && (req.user.role === 'owner' || req.user.role === 'admin')) {
			next();
		} else if (role === 'owner' && req.user.role === 'owner') {
			next();
		}	else {
			//res.send('Unauthorized', 401);
			res.json({
				success: false,
				message: 'Unauthorized.'
			});
		}
	};
}
module.exports = privilegesOnly;
