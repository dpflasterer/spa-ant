/**
 * Created by David on 4/6/14.
 */

// route middleware to make sure a user is logged in
function loggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}

	// make sure session is destroyed
	req.session.destroy();
	// let the client know why
	res.json({
		success: false,
		message: 'Not logged in.'
	});
}
module.exports = loggedIn;
