/**
 * Created by David on 4/6/14.
 */

// restricts privileges to self or privileges higher than user
function restrictPrivileges(req, res, next) {
	if (req.session.user.id === req.loadedUser.id ||	req.user.roleLevel() > req.loadedUser.roleLevel()) {
		next();
	} else {
		res.send('Unauthorized', 401);
		//res.json({
		//  success: false,
		//  message: 'Unauthorized.'
		//});

	}
}
module.exports = restrictPrivileges;
