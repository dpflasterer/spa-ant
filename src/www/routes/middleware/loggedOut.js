/**
 * Created by David on 12/1/13.
 */
function loggedOut(req, res, next) {
	if (!req.isAuthenticated()) {
		return next();
	}

	res.json({
		success: false,
		message: 'Logged In.'
	});
}

module.exports = loggedOut;
