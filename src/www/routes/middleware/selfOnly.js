/**
 * Created by David on 12/2/13.
 */
function selfOnly(req, res, next) {
	if (!req.isAuthenticated() || req.session.user.id !== req.user.id) {
		res.send('Unauthorized', 401);
		//res.json({
		//  success: false,
		//  message: 'Unauthorized.'
		//});
	} else {
		next();
	}
}

module.exports = selfOnly;
