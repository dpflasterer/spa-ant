/**
 * Created by David on 4/6/14.
 */

// route middleware to make sure a user is logged in
function selfOrPrivileges(role) {
	return function(req, res, next) {
		if (req.session.user.id === req.loadedUser.id) {
			next();
		}	else if (role === 'admin' && (req.user.role === 'owner' || req.user.role === 'admin')) {
			next();
		}	else if (role === 'owner'	&& req.user.role === 'owner') {
			next();
		}	else {
			res.send('Unauthorized', 401);
			//res.json({
			//  success: false,
			//  message: 'Unauthorized.'
			//});
		}
	};
}
module.exports = selfOrPrivileges;
