/*
 * Session Routes
*/
var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('../../data/models/user');
var loggedOut = require('./middleware/loggedOut');
var loggedIn = require('./middleware/loggedIn');

router.use(function timeLog(req, res, next) {
	console.log('Time: ', Date.now());
	next();
});

router.post('/', loggedOut, function(req, res, next) {
	passport.authenticate('local-login', function(err, user, info) {
		if (err) { return next(err); }
		if (!user) {
			return res.json({
				success: false,
				message: req.flash('loginMessage')[0]
			});
		}
		req.logIn(user, function(err) {
			if (err) { return next(err); }
			return res.json({
				success: true,
				user: user
			});
		});
	})(req, res, next);
});

router.get('/', loggedIn, function(req, res, next) {
	if (req.user) {
		return res.json({
			success: true,
			user: {
				name: req.user.name,
				username: req.user.username,
				email: req.user.email,
				role: req.user.role,
				btceAPI: req.user.btceAPI
			}
		});
	}
});

router.delete('/', function(req, res/*, not used, next*/) {
	req.logout();
	req.session.destroy();
	res.json({success: true});
});
router.get('/end', function(req, res/*, not used, next*/) {
	req.logout();
	req.session.destroy();
	res.json({success: true});
});

module.exports = router;
