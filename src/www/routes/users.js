
/*
 * User Routes
 */
var express = require('express');
var router = express.Router();
var passport = require('passport');
var async = require('async');
var User = require('../../data/models/user');
var loadUser = require('./middleware/loadUser');
var loggedOut = require('./middleware/loggedOut');
var loggedIn = require('./middleware/loggedIn');
var selfOrPrivileges = require('./middleware/selfOrPrivileges');
var restrictPrivileges = require('./middleware/restrictPrivileges');
var privilegesOnly = require('./middleware/privilegesOnly');

router.use(function timeLog(req, res, next) {
	console.log('Time: ', Date.now());
	next();
});

router.post('/new', loggedOut, function(req, res, next) {
	passport.authenticate('local-signup', function(err, user, info) {
		if (err) { return next(err); }
		if (!user) {
			return res.json({
				success: false,
				message: req.flash('signupMessage')[0]
			});
		}
		req.logIn(user, function(err) {
			if (err) { return next(err); }
			return res.json({
				success: true,
				user: {
					name: req.user.name,
					username: req.user.username,
					email: req.user.email,
					role: req.user.role,
					btceAPI: req.user.btceAPI
				}
			});
		});
	})(req, res, next);
});

router.get('/', loggedIn, privilegesOnly('admin'), function(req, res, next) {

	User
		.find({})
		//.where('_id').gt(req.query.page || '0')

		// todo: add pagination
		.sort({_id: 1})
		//.limit(3)
		.lean()
		.exec(function(err, users) {
			if (err) {
				return next(err);
			}
			async.each(users, sanitize, respond);

			function sanitize(user, callback) {
				delete user.__v;
				delete user.password;
				callback();
			}

			function respond(err) {
				if (err) {
					res.json({
						success: false
					});
				} else {
					res.json({
						success: true,
						users: users
					});
				}
			}
		});
});

router.get('/:name', loggedIn, loadUser, selfOrPrivileges('admin'), function(req, res, next) {
	res.json({
		success: true,
		loadedUser: {
			name: req.loadedUser.name,
			username: req.loadedUser.username,
			email: req.loadedUser.email,
			role: req.loadedUser.role,
			btceAPI: req.loadedUser.btceAPI
		}
	});
});

router.post('/:name', loggedIn, loadUser,
	selfOrPrivileges('admin'), restrictPrivileges, function(req, res, next) {
	if (
		req.body.password &&
		req.body.password !== req.body.password2
	) {
		return res.json({
			success: false,
			message: 'Oops, your passwords don\'t match.'
		});
	}

	// find a user whose email is the same as the forms email
	// we are checking to see if the user trying to login already exists
	/* jshint ignore:start */
	User.findOne({'username' :  req.loadedUser.username}, function(err, user) {
		// if there are any errors, return the error
		if (err) {
			//return next(err);
			return res.json({
				success: false,
				message: err
			});
		}

		// check to see if theres already a user with that email
		if (user) {

			// prevent user from changing their own role.
			if (user.name === req.body.name && user.body) {
				delete user.body.role;
			}

			// set the user's local credentials
			user.name = req.body.name || user.name;
			user.email = req.body.email || user.email;
			user.role = req.body.role || user.role;
			user.btceAPI = req.body.btceAPI || user.role;

			if (req.body.password != null) {
				user.password = user.generateHash(req.body.password);
			}

			// save the user
			user.save(function(err) {
				if (err) { console.log(err); } // todo: try to prevent all errors
				req.session.user = {
					'id': user._id,
					'username': user.username,
					'name': user.name
				};
				res.json({
					success: true,
					message: 'Profile Updated.',
					user: {
						name: user.name,
						username: user.username,
						email: user.email,
						role: user.role,
						btceAPI: user.btceAPI
					}
				});
			});

		} else {

			//return next(null, false, req.flash('signupMessage', 'That email is already taken.'));
			return res.json({
				success: false,
				message: 'User unknown.'
			});

		}
	});
	/* jshint ignore:end */

});

router.delete('/:name', loadUser, selfOrPrivileges('admin'), restrictPrivileges, function(req, res, next) {
	req.loadedUser.remove(function(err) {
		if (err) {
			return next(err);
		}
		res.json({
			success: true
		});
	});
});

module.exports = router;
