module.exports = function() {
	var client = './src/client/';
	var server = './src/www/';
	var report = './report/';
	var specRunnerFile = 'specs.html';
	var temp = './dev/';
	var wiredep = require('wiredep');
	var bowerFiles = wiredep({devDependencies: true})['js'];
	var bower = {
		json: require('./bower.json'),
		directory: './bower_components/',
		ignorePath: '../..'
	};
	var nodeModules = 'node_modules';

	var config = {
		/**
     * File paths
     */
		// all javascript that we want to vet
		alljs: [
			client + '**/*.js',
			'!' + client + 'templates.js',
			'!' + client + '_templates/*.*',
			server + '**/*.js'
		],
		build: './public/',

		client: client,
		css: temp + 'styles.css',
		fonts: [
			bower.directory + 'font-awesome/fonts/**/*.*',
			bower.directory + 'bootstrap/fonts/**/*.*'
		],
		//html: client + '**/*.html',
		images: client + 'images/**/*.*',
		index: {
			html: temp + 'index.html',
			jade: client + 'index.jade'
		},
		jade: [
			client + '**/*.jade',
			'!' + client + 'index.jade'
		],
		jadeIndex: client + 'index.jade',

		// app js, with no specs
		js: [
			client + '**/*.js',
			//client + '**/*.module.js',
			//client + '**/*.js',
			'!' + client + '**/*.spec.js',
			'!' + client + '_templates/*.js'
		],
		jsOrder: [
			'app.js',
			'**/*.module.js',
			'**/*.js'
		],
		less: client + '_less/styles.less',
		allLess: client + '_less/*.less',
		report: report,
		server: server,
		source: 'client/',
		stubsjs: [
			bower.directory + 'angular-mocks/angular-mocks.js',
			client + 'stubs/**/*.js'
		],
		temp: temp,

		modules: [
			client + 'app.js',
			client + '**/*.module.js'
		],

		/**
		 * optimized files
		 */
		optimized: {
			app: 'app.js',
			lib: 'lib.js'
		},
		//
		///**
		// * plato
		// */
		//plato: {js: clientApp + '**/*.js'},

		/**
		 * browser sync
		 */
		browserReloadDelay: 1200,

		/**
		 * template cache
		 */
		templateCache: {
			file: 'templates.js',
			options: {
				module: 'app.templates',
				root: '',
				standalone: true
			}
		},
		/**
		 * Bower and NPM files
		 */
		bower: bower,
		//packages: [
		//    './package.json',
		//    './bower.json'
		//],
		//
		/**
		 * specs.html, our HTML spec runner
		 */
		specRunner: client + specRunnerFile,
		specRunnerFile: specRunnerFile,

		/**
		 * The sequence of the injections into specs.html:
		 *  1 testlibraries
		 *      mocha setup
		 *  2 bower
		 *  3 js
		 *  4 spechelpers
		 *  5 specs
		 *  6 templates
		 */
		testlibraries: [
			nodeModules + '/mocha/mocha.js',
			nodeModules + '/chai/chai.js',
			nodeModules + '/mocha-clean/index.js',
			nodeModules + '/sinon-chai/lib/sinon-chai.js'
		],
		//specHelpers: [client + 'test-helpers/*.js'],
		specs: [client + '**/*.spec.js'],
		//serverIntegrationSpecs: [client + '/tests/server-integration/**/*.spec.js'],

		/**
		 * Node settings
		 */
		nodeServer: server + 'app.js',
		defaultPort: '8001'
	};

	/**
	     * wiredep and bower settings
	     */
	config.getWiredepDefaultOptions = function() {
		var options = {
			bowerJson: config.bower.json,
			directory: config.bower.directory,
			ignorePath: config.bower.ignorePath
		};
		return options;
	};

	/**
	     * karma settings
	     */
	config.karma = getKarmaOptions();
	//
	return config;

	////////////////

	function getKarmaOptions() {
		var options = {
			files: [].concat(
				bowerFiles,
				//config.specHelpers,
				//client + '**/*.module.js',
				client + 'app.js',
				client + '**/*.js'/*,
				client + config.templateCache.file,
				config.serverIntegrationSpecs*/
			),
			exclude: [],
			coverage: {
				dir: report + 'coverage',
				reporters: [
					// reporters not supporting the `file` property
					{type: 'html', subdir: 'report-html'},
					{type: 'lcov', subdir: 'report-lcov'},
					{type: 'text-summary'} //, subdir: '.', file: 'text-summary.txt'}
				]
			},
			preprocessors: {}
		};
		options.preprocessors[client + '**/!(*.spec)+(.js)'] = ['coverage'];
		return options;
	}
};
